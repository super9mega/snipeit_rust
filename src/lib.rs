use reqwest;
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};

pub struct SnipeItClient {
    base_url: String,
    api_key: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Model {
    pub id: u32,
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Location {
    pub id: u32,
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Audit {
    pub asset_tag: String,
    pub location_id: u32,
    pub next_audit_date: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Asset {
    pub asset_tag: String,
    pub serial: String,
    pub notes: Option<String>,
    pub model: Model,
    pub id: u32,
    pub rtd_location: Option<Location>,
    pub location: Option<Location>
    // Add more fields as needed
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UpdateAsset {
    pub asset_tag: String,
    pub serial: String,
    pub notes: Option<String>,
    pub model: Model,
    pub id: u32,
    pub rtd_location_id: Option<u32>
    // Add more fields as needed
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AddAsset {
    pub asset_tag: String,
    pub status_id: u32,
    pub model_id: u32,
    pub serial: String,
    pub purchase_date: String,
    pub warranty_months: u32
}
#[derive(Debug, Serialize, Deserialize)]
pub struct AssetList {
    pub total: u32,
    pub rows: Vec<Asset>
}

impl Asset {
    pub fn to_update(&self) -> UpdateAsset {
        UpdateAsset {
            asset_tag: (*self.asset_tag.to_string()).parse().unwrap(),
            serial: (*self.serial.to_string()).parse().unwrap(),
            id: self.id,
            model: self.model.clone(),
            notes: self.notes.clone(),
            rtd_location_id: match self.rtd_location.clone() {
                Some(t) => Some(t.id),
                None => None
            }
        }
    }
}

impl Model {
    pub fn new(id: u32, name: String) -> Model {
        Model {
            id,
            name
        }
    }
}

impl SnipeItClient {
    pub fn new(base_url: &str, api_key: &str) -> Self {
        SnipeItClient {
            base_url: base_url.to_string() + "/api/v1",
            api_key: api_key.to_string(),
        }
    }

    pub fn get_all_assets(&self) -> Result<AssetList, reqwest::Error> {
        let url = format!("{}/hardware", &self.base_url);
        let client = reqwest::blocking::Client::new();
        let response = client
            .get(&url)
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .header("Authorization", format!("Bearer {}", &self.api_key))
            .send()?;

        let asset: AssetList = response.json()?;
        Ok(asset)
    }
    pub fn get_asset_by_tag(&self, asset_tag: &str) -> Result<Asset, reqwest::Error> {
        let url = format!("{}/hardware/bytag/{}?deleted=false", &self.base_url, asset_tag);
        let client = reqwest::blocking::Client::new();
        let response = client
            .get(&url)
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .header("Authorization", format!("Bearer {}", &self.api_key))
            .send()?;

        let asset: Asset = response.json()?;
        Ok(asset)
    }
    pub fn get_asset_by_id(&self, id: u32) -> Result<Asset, reqwest::Error> {
        let url = format!("{}/hardware/{}", &self.base_url, id);
        let client = reqwest::blocking::Client::new();
        let response = client
            .get(&url)
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .header("Authorization", format!("Bearer {}", &self.api_key))
            .send()?;

        let asset: Asset = response.json()?;
        Ok(asset)
    }
    pub fn get_location_byid(&self, location_id: u32) -> Result<Location, reqwest::Error> {
        let url = format!("{}/locations/{}", &self.base_url, location_id);
        let client = reqwest::blocking::Client::new();
        let response = client
            .get(&url)
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .header("Authorization", format!("Bearer {}", &self.api_key))
            .send()?;

        let location: Location = response.json()?;
        Ok(location)
    }
    pub fn audit_asset(&self, audit_info: Audit) -> Result<StatusCode, reqwest::Error> {
        let url = format!("{}/hardware/audit", &self.base_url);
        let client = reqwest::blocking::Client::new();
        let response = client
            .post(&url)
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .header("Authorization", format!("Bearer {}", &self.api_key))
            .json(&audit_info)
            .send()?;

        let confirm = response.status();
        Ok(confirm)
    }
    pub fn update_asset(&self, update_asset: UpdateAsset) -> Result<StatusCode, reqwest::Error> {
        let url = format!("{}/hardware/{}", &self.base_url, update_asset.id);
        let update_asset = UpdateAsset {
            asset_tag: update_asset.asset_tag,
            serial: update_asset.serial,
            id: update_asset.id,
            model: update_asset.model,
            notes: update_asset.notes,
            rtd_location_id: update_asset.rtd_location_id
        };
        let client = reqwest::blocking::Client::new();
        let response = client
            .patch(&url)
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .header("Authorization", format!("Bearer {}", &self.api_key))
            .json(&update_asset)
            .send()?;

        let confirm = response.status();
        Ok(confirm)
    }
    pub fn restore_asset(&self, id: u32) -> Result<StatusCode, reqwest::Error> {
        let url = format!("{}/hardware/{}/restore", &self.base_url, id);
        let client = reqwest::blocking::Client::new();
        let response = client
            .post(&url)
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .header("Authorization", format!("Bearer {}", &self.api_key))
            .send()?;

        let confirm = response.status();
        Ok(confirm)
    }
    pub fn delete_asset(&self, id: u32) -> Result<StatusCode, reqwest::Error> {
        let url = format!("{}/hardware/{}", &self.base_url, id);
        let client = reqwest::blocking::Client::new();
        let response = client
            .delete(&url)
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .header("Authorization", format!("Bearer {}", &self.api_key))
            .send()?;

        let confirm = response.status();
        Ok(confirm)
    }
    pub fn get_overdue_assets(&self) -> Result<AssetList, reqwest::Error> {
        let url = format!("{}/hardware/audit/overdue", &self.base_url);
        let client = reqwest::blocking::Client::new();
        let response = client
            .get(&url)
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .header("Authorization", format!("Bearer {}", &self.api_key))
            .send()?;

        let asset: AssetList = response.json()?;
        Ok(asset)
    }
    pub fn add_asset(&self, asset: AddAsset) -> Result<StatusCode, reqwest::Error> {
        let url = format!("{}/hardware", &self.base_url);
        let client = reqwest::blocking::Client::new();
        let response = client
            .post(&url)
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .header("Authorization", format!("Bearer {}", &self.api_key))
            .json(&asset)
            .send()?;

        let confirm = response.status();
        Ok(confirm)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_asset_bytag_serial() {
        let client = SnipeItClient::new("http://172.16.6.41", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2QwYmM3Yjc4ZTc5MGRhOTc0Mzc0ZDk4YmIxNjIxODAxOGYxMWUzZTk0NDQzNWY0ZThjMDk0NDI4Y2NiNGJhYzE0ZGRkOWEyNjNlYjU4YWUiLCJpYXQiOjE2OTc1NjU1NDcuNzIwNjQxLCJuYmYiOjE2OTc1NjU1NDcuNzIwNjQzLCJleHAiOjIzMjg3MTc1NDcuNzExNjU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bgXIGVSSCptWWcpGLwUuq-LcFAu_uSwdebzshMAni6hX2V2NTn-5GsHxTkL5n2nlZaXWNpw216z9Uhp3PXwPLAflkeHIxiKYeYbf8SEnhogkGwLfuri8cqFV4JI6N-cZwKmhAgK-WyCpr5fXXxQNUlxvQz4XWCkiAPBE0NDHZGTJKzk1ytFwa6zxOIIuXpA7RCOSrYBq7pn67ezJp1moSF2APrT0SbAP4UP4maEVDfmRzEfMhzoFxsJj_K8rkdYo6rPekSB3OedjtBt2EvblBfLpe7gIg4VvgROd_vAPoHLf7FapyAaxcXQWUKPE1Pc4dSYmB5tWcTZ62LKVPTNGYL23m4CIIq9xBEZzJtZ--kgmTkG97DDL7N66rxs-w9hSMJYw3AXSvL8HL0xXUMSihDbnaKLzyLuIt_4LHkXL3vjqv9G7yRQQulTkdA7zPAQuGLYkFg1AQ5xx5Gne7nT_TEffjDWlCSVJJ9hZL2jtbDqm7hGAP2FS0bmqRKQhEBBpW3SldIok4wn7xc5mQKeHVoBR0ZNvnbCo8wQGKfOerHbpLjxVQ1GB7M4N_pdB-1xYr1Yf6Ri15ILkh0Sei6-8XHTDguW4FFPOu24YQkqClXW8qntlQAYQSqyLh46FamiMKY_taGLUDM2Uf-Yg54ePbHDZSFncXNYUcCRWrqW44Xc");
        let asset = client.get_asset_by_tag("000000007979").unwrap();
        assert_eq!(asset.serial, "3VLJQ92");
    }
    #[test]
    fn get_asset_bytag_model() {
        let client = SnipeItClient::new("http://172.16.6.41", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2QwYmM3Yjc4ZTc5MGRhOTc0Mzc0ZDk4YmIxNjIxODAxOGYxMWUzZTk0NDQzNWY0ZThjMDk0NDI4Y2NiNGJhYzE0ZGRkOWEyNjNlYjU4YWUiLCJpYXQiOjE2OTc1NjU1NDcuNzIwNjQxLCJuYmYiOjE2OTc1NjU1NDcuNzIwNjQzLCJleHAiOjIzMjg3MTc1NDcuNzExNjU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bgXIGVSSCptWWcpGLwUuq-LcFAu_uSwdebzshMAni6hX2V2NTn-5GsHxTkL5n2nlZaXWNpw216z9Uhp3PXwPLAflkeHIxiKYeYbf8SEnhogkGwLfuri8cqFV4JI6N-cZwKmhAgK-WyCpr5fXXxQNUlxvQz4XWCkiAPBE0NDHZGTJKzk1ytFwa6zxOIIuXpA7RCOSrYBq7pn67ezJp1moSF2APrT0SbAP4UP4maEVDfmRzEfMhzoFxsJj_K8rkdYo6rPekSB3OedjtBt2EvblBfLpe7gIg4VvgROd_vAPoHLf7FapyAaxcXQWUKPE1Pc4dSYmB5tWcTZ62LKVPTNGYL23m4CIIq9xBEZzJtZ--kgmTkG97DDL7N66rxs-w9hSMJYw3AXSvL8HL0xXUMSihDbnaKLzyLuIt_4LHkXL3vjqv9G7yRQQulTkdA7zPAQuGLYkFg1AQ5xx5Gne7nT_TEffjDWlCSVJJ9hZL2jtbDqm7hGAP2FS0bmqRKQhEBBpW3SldIok4wn7xc5mQKeHVoBR0ZNvnbCo8wQGKfOerHbpLjxVQ1GB7M4N_pdB-1xYr1Yf6Ri15ILkh0Sei6-8XHTDguW4FFPOu24YQkqClXW8qntlQAYQSqyLh46FamiMKY_taGLUDM2Uf-Yg54ePbHDZSFncXNYUcCRWrqW44Xc");
        let asset = client.get_asset_by_tag("000000007979").unwrap();
        assert_eq!(asset.model.name, "Latitude 5470");
    }
    #[test]
    fn get_asset_bytag_id() {
        let client = SnipeItClient::new("http://172.16.6.41", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2QwYmM3Yjc4ZTc5MGRhOTc0Mzc0ZDk4YmIxNjIxODAxOGYxMWUzZTk0NDQzNWY0ZThjMDk0NDI4Y2NiNGJhYzE0ZGRkOWEyNjNlYjU4YWUiLCJpYXQiOjE2OTc1NjU1NDcuNzIwNjQxLCJuYmYiOjE2OTc1NjU1NDcuNzIwNjQzLCJleHAiOjIzMjg3MTc1NDcuNzExNjU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bgXIGVSSCptWWcpGLwUuq-LcFAu_uSwdebzshMAni6hX2V2NTn-5GsHxTkL5n2nlZaXWNpw216z9Uhp3PXwPLAflkeHIxiKYeYbf8SEnhogkGwLfuri8cqFV4JI6N-cZwKmhAgK-WyCpr5fXXxQNUlxvQz4XWCkiAPBE0NDHZGTJKzk1ytFwa6zxOIIuXpA7RCOSrYBq7pn67ezJp1moSF2APrT0SbAP4UP4maEVDfmRzEfMhzoFxsJj_K8rkdYo6rPekSB3OedjtBt2EvblBfLpe7gIg4VvgROd_vAPoHLf7FapyAaxcXQWUKPE1Pc4dSYmB5tWcTZ62LKVPTNGYL23m4CIIq9xBEZzJtZ--kgmTkG97DDL7N66rxs-w9hSMJYw3AXSvL8HL0xXUMSihDbnaKLzyLuIt_4LHkXL3vjqv9G7yRQQulTkdA7zPAQuGLYkFg1AQ5xx5Gne7nT_TEffjDWlCSVJJ9hZL2jtbDqm7hGAP2FS0bmqRKQhEBBpW3SldIok4wn7xc5mQKeHVoBR0ZNvnbCo8wQGKfOerHbpLjxVQ1GB7M4N_pdB-1xYr1Yf6Ri15ILkh0Sei6-8XHTDguW4FFPOu24YQkqClXW8qntlQAYQSqyLh46FamiMKY_taGLUDM2Uf-Yg54ePbHDZSFncXNYUcCRWrqW44Xc");
        let asset = client.get_asset_by_tag("000000007979").unwrap();
        assert_eq!(asset.id, 107);
        assert_ne!(asset.id, 253)
    }
    #[test]
    fn get_asset_byid_serial() {
        let client = SnipeItClient::new("http://172.16.6.41", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2QwYmM3Yjc4ZTc5MGRhOTc0Mzc0ZDk4YmIxNjIxODAxOGYxMWUzZTk0NDQzNWY0ZThjMDk0NDI4Y2NiNGJhYzE0ZGRkOWEyNjNlYjU4YWUiLCJpYXQiOjE2OTc1NjU1NDcuNzIwNjQxLCJuYmYiOjE2OTc1NjU1NDcuNzIwNjQzLCJleHAiOjIzMjg3MTc1NDcuNzExNjU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bgXIGVSSCptWWcpGLwUuq-LcFAu_uSwdebzshMAni6hX2V2NTn-5GsHxTkL5n2nlZaXWNpw216z9Uhp3PXwPLAflkeHIxiKYeYbf8SEnhogkGwLfuri8cqFV4JI6N-cZwKmhAgK-WyCpr5fXXxQNUlxvQz4XWCkiAPBE0NDHZGTJKzk1ytFwa6zxOIIuXpA7RCOSrYBq7pn67ezJp1moSF2APrT0SbAP4UP4maEVDfmRzEfMhzoFxsJj_K8rkdYo6rPekSB3OedjtBt2EvblBfLpe7gIg4VvgROd_vAPoHLf7FapyAaxcXQWUKPE1Pc4dSYmB5tWcTZ62LKVPTNGYL23m4CIIq9xBEZzJtZ--kgmTkG97DDL7N66rxs-w9hSMJYw3AXSvL8HL0xXUMSihDbnaKLzyLuIt_4LHkXL3vjqv9G7yRQQulTkdA7zPAQuGLYkFg1AQ5xx5Gne7nT_TEffjDWlCSVJJ9hZL2jtbDqm7hGAP2FS0bmqRKQhEBBpW3SldIok4wn7xc5mQKeHVoBR0ZNvnbCo8wQGKfOerHbpLjxVQ1GB7M4N_pdB-1xYr1Yf6Ri15ILkh0Sei6-8XHTDguW4FFPOu24YQkqClXW8qntlQAYQSqyLh46FamiMKY_taGLUDM2Uf-Yg54ePbHDZSFncXNYUcCRWrqW44Xc");
        let asset = client.get_asset_by_id(107).unwrap();
        assert_eq!(asset.serial, "3VLJQ92");
    }
    #[test]
    fn get_asset_byid_model() {
        let client = SnipeItClient::new("http://172.16.6.41", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2QwYmM3Yjc4ZTc5MGRhOTc0Mzc0ZDk4YmIxNjIxODAxOGYxMWUzZTk0NDQzNWY0ZThjMDk0NDI4Y2NiNGJhYzE0ZGRkOWEyNjNlYjU4YWUiLCJpYXQiOjE2OTc1NjU1NDcuNzIwNjQxLCJuYmYiOjE2OTc1NjU1NDcuNzIwNjQzLCJleHAiOjIzMjg3MTc1NDcuNzExNjU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bgXIGVSSCptWWcpGLwUuq-LcFAu_uSwdebzshMAni6hX2V2NTn-5GsHxTkL5n2nlZaXWNpw216z9Uhp3PXwPLAflkeHIxiKYeYbf8SEnhogkGwLfuri8cqFV4JI6N-cZwKmhAgK-WyCpr5fXXxQNUlxvQz4XWCkiAPBE0NDHZGTJKzk1ytFwa6zxOIIuXpA7RCOSrYBq7pn67ezJp1moSF2APrT0SbAP4UP4maEVDfmRzEfMhzoFxsJj_K8rkdYo6rPekSB3OedjtBt2EvblBfLpe7gIg4VvgROd_vAPoHLf7FapyAaxcXQWUKPE1Pc4dSYmB5tWcTZ62LKVPTNGYL23m4CIIq9xBEZzJtZ--kgmTkG97DDL7N66rxs-w9hSMJYw3AXSvL8HL0xXUMSihDbnaKLzyLuIt_4LHkXL3vjqv9G7yRQQulTkdA7zPAQuGLYkFg1AQ5xx5Gne7nT_TEffjDWlCSVJJ9hZL2jtbDqm7hGAP2FS0bmqRKQhEBBpW3SldIok4wn7xc5mQKeHVoBR0ZNvnbCo8wQGKfOerHbpLjxVQ1GB7M4N_pdB-1xYr1Yf6Ri15ILkh0Sei6-8XHTDguW4FFPOu24YQkqClXW8qntlQAYQSqyLh46FamiMKY_taGLUDM2Uf-Yg54ePbHDZSFncXNYUcCRWrqW44Xc");
        let asset = client.get_asset_by_id(107).unwrap();
        assert_eq!(asset.model.name, "Latitude 5470");
    }
    #[test]
    fn get_asset_byid_id() {
        let client = SnipeItClient::new("http://172.16.6.41", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2QwYmM3Yjc4ZTc5MGRhOTc0Mzc0ZDk4YmIxNjIxODAxOGYxMWUzZTk0NDQzNWY0ZThjMDk0NDI4Y2NiNGJhYzE0ZGRkOWEyNjNlYjU4YWUiLCJpYXQiOjE2OTc1NjU1NDcuNzIwNjQxLCJuYmYiOjE2OTc1NjU1NDcuNzIwNjQzLCJleHAiOjIzMjg3MTc1NDcuNzExNjU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bgXIGVSSCptWWcpGLwUuq-LcFAu_uSwdebzshMAni6hX2V2NTn-5GsHxTkL5n2nlZaXWNpw216z9Uhp3PXwPLAflkeHIxiKYeYbf8SEnhogkGwLfuri8cqFV4JI6N-cZwKmhAgK-WyCpr5fXXxQNUlxvQz4XWCkiAPBE0NDHZGTJKzk1ytFwa6zxOIIuXpA7RCOSrYBq7pn67ezJp1moSF2APrT0SbAP4UP4maEVDfmRzEfMhzoFxsJj_K8rkdYo6rPekSB3OedjtBt2EvblBfLpe7gIg4VvgROd_vAPoHLf7FapyAaxcXQWUKPE1Pc4dSYmB5tWcTZ62LKVPTNGYL23m4CIIq9xBEZzJtZ--kgmTkG97DDL7N66rxs-w9hSMJYw3AXSvL8HL0xXUMSihDbnaKLzyLuIt_4LHkXL3vjqv9G7yRQQulTkdA7zPAQuGLYkFg1AQ5xx5Gne7nT_TEffjDWlCSVJJ9hZL2jtbDqm7hGAP2FS0bmqRKQhEBBpW3SldIok4wn7xc5mQKeHVoBR0ZNvnbCo8wQGKfOerHbpLjxVQ1GB7M4N_pdB-1xYr1Yf6Ri15ILkh0Sei6-8XHTDguW4FFPOu24YQkqClXW8qntlQAYQSqyLh46FamiMKY_taGLUDM2Uf-Yg54ePbHDZSFncXNYUcCRWrqW44Xc");
        let asset = client.get_asset_by_id(107).unwrap();
        assert_eq!(asset.id, 107);
        assert_ne!(asset.id, 253)
    }
    #[test]
    fn audit_asset() {
        let client = SnipeItClient::new("http://172.16.6.41", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2QwYmM3Yjc4ZTc5MGRhOTc0Mzc0ZDk4YmIxNjIxODAxOGYxMWUzZTk0NDQzNWY0ZThjMDk0NDI4Y2NiNGJhYzE0ZGRkOWEyNjNlYjU4YWUiLCJpYXQiOjE2OTc1NjU1NDcuNzIwNjQxLCJuYmYiOjE2OTc1NjU1NDcuNzIwNjQzLCJleHAiOjIzMjg3MTc1NDcuNzExNjU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bgXIGVSSCptWWcpGLwUuq-LcFAu_uSwdebzshMAni6hX2V2NTn-5GsHxTkL5n2nlZaXWNpw216z9Uhp3PXwPLAflkeHIxiKYeYbf8SEnhogkGwLfuri8cqFV4JI6N-cZwKmhAgK-WyCpr5fXXxQNUlxvQz4XWCkiAPBE0NDHZGTJKzk1ytFwa6zxOIIuXpA7RCOSrYBq7pn67ezJp1moSF2APrT0SbAP4UP4maEVDfmRzEfMhzoFxsJj_K8rkdYo6rPekSB3OedjtBt2EvblBfLpe7gIg4VvgROd_vAPoHLf7FapyAaxcXQWUKPE1Pc4dSYmB5tWcTZ62LKVPTNGYL23m4CIIq9xBEZzJtZ--kgmTkG97DDL7N66rxs-w9hSMJYw3AXSvL8HL0xXUMSihDbnaKLzyLuIt_4LHkXL3vjqv9G7yRQQulTkdA7zPAQuGLYkFg1AQ5xx5Gne7nT_TEffjDWlCSVJJ9hZL2jtbDqm7hGAP2FS0bmqRKQhEBBpW3SldIok4wn7xc5mQKeHVoBR0ZNvnbCo8wQGKfOerHbpLjxVQ1GB7M4N_pdB-1xYr1Yf6Ri15ILkh0Sei6-8XHTDguW4FFPOu24YQkqClXW8qntlQAYQSqyLh46FamiMKY_taGLUDM2Uf-Yg54ePbHDZSFncXNYUcCRWrqW44Xc");
        let audit_info = Audit {
            asset_tag: "000000007979".to_string(),
            location_id: 8,
            next_audit_date: "2023-11-02".to_string()
        };
        let audit_status = client.audit_asset(audit_info).unwrap();
        assert_eq!(StatusCode::from_u16(200).unwrap(), audit_status);
    }
    #[test]
    fn get_assets_byoverdue() {
        let client = SnipeItClient::new("http://172.16.6.41", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2QwYmM3Yjc4ZTc5MGRhOTc0Mzc0ZDk4YmIxNjIxODAxOGYxMWUzZTk0NDQzNWY0ZThjMDk0NDI4Y2NiNGJhYzE0ZGRkOWEyNjNlYjU4YWUiLCJpYXQiOjE2OTc1NjU1NDcuNzIwNjQxLCJuYmYiOjE2OTc1NjU1NDcuNzIwNjQzLCJleHAiOjIzMjg3MTc1NDcuNzExNjU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bgXIGVSSCptWWcpGLwUuq-LcFAu_uSwdebzshMAni6hX2V2NTn-5GsHxTkL5n2nlZaXWNpw216z9Uhp3PXwPLAflkeHIxiKYeYbf8SEnhogkGwLfuri8cqFV4JI6N-cZwKmhAgK-WyCpr5fXXxQNUlxvQz4XWCkiAPBE0NDHZGTJKzk1ytFwa6zxOIIuXpA7RCOSrYBq7pn67ezJp1moSF2APrT0SbAP4UP4maEVDfmRzEfMhzoFxsJj_K8rkdYo6rPekSB3OedjtBt2EvblBfLpe7gIg4VvgROd_vAPoHLf7FapyAaxcXQWUKPE1Pc4dSYmB5tWcTZ62LKVPTNGYL23m4CIIq9xBEZzJtZ--kgmTkG97DDL7N66rxs-w9hSMJYw3AXSvL8HL0xXUMSihDbnaKLzyLuIt_4LHkXL3vjqv9G7yRQQulTkdA7zPAQuGLYkFg1AQ5xx5Gne7nT_TEffjDWlCSVJJ9hZL2jtbDqm7hGAP2FS0bmqRKQhEBBpW3SldIok4wn7xc5mQKeHVoBR0ZNvnbCo8wQGKfOerHbpLjxVQ1GB7M4N_pdB-1xYr1Yf6Ri15ILkh0Sei6-8XHTDguW4FFPOu24YQkqClXW8qntlQAYQSqyLh46FamiMKY_taGLUDM2Uf-Yg54ePbHDZSFncXNYUcCRWrqW44Xc");
        let _asset = client.get_overdue_assets().unwrap();
    }
    #[test]
    fn get_all_assets() {
        let client = SnipeItClient::new("http://172.16.6.41", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2QwYmM3Yjc4ZTc5MGRhOTc0Mzc0ZDk4YmIxNjIxODAxOGYxMWUzZTk0NDQzNWY0ZThjMDk0NDI4Y2NiNGJhYzE0ZGRkOWEyNjNlYjU4YWUiLCJpYXQiOjE2OTc1NjU1NDcuNzIwNjQxLCJuYmYiOjE2OTc1NjU1NDcuNzIwNjQzLCJleHAiOjIzMjg3MTc1NDcuNzExNjU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bgXIGVSSCptWWcpGLwUuq-LcFAu_uSwdebzshMAni6hX2V2NTn-5GsHxTkL5n2nlZaXWNpw216z9Uhp3PXwPLAflkeHIxiKYeYbf8SEnhogkGwLfuri8cqFV4JI6N-cZwKmhAgK-WyCpr5fXXxQNUlxvQz4XWCkiAPBE0NDHZGTJKzk1ytFwa6zxOIIuXpA7RCOSrYBq7pn67ezJp1moSF2APrT0SbAP4UP4maEVDfmRzEfMhzoFxsJj_K8rkdYo6rPekSB3OedjtBt2EvblBfLpe7gIg4VvgROd_vAPoHLf7FapyAaxcXQWUKPE1Pc4dSYmB5tWcTZ62LKVPTNGYL23m4CIIq9xBEZzJtZ--kgmTkG97DDL7N66rxs-w9hSMJYw3AXSvL8HL0xXUMSihDbnaKLzyLuIt_4LHkXL3vjqv9G7yRQQulTkdA7zPAQuGLYkFg1AQ5xx5Gne7nT_TEffjDWlCSVJJ9hZL2jtbDqm7hGAP2FS0bmqRKQhEBBpW3SldIok4wn7xc5mQKeHVoBR0ZNvnbCo8wQGKfOerHbpLjxVQ1GB7M4N_pdB-1xYr1Yf6Ri15ILkh0Sei6-8XHTDguW4FFPOu24YQkqClXW8qntlQAYQSqyLh46FamiMKY_taGLUDM2Uf-Yg54ePbHDZSFncXNYUcCRWrqW44Xc");
        let _asset = client.get_all_assets().unwrap();
    }
}
